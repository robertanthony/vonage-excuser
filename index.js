import express from "express";
import bodyParser from "body-parser";
import cors from 'cors'

import { startJobs } from "./cron/index.js";
import answer from "./routes/answer.js";
import events from "./routes/events.js";
import jobs from "./routes/jobs.js";
import processVoice from "./routes/processVoice.js";

const { SERVER_PORT } = process.env;

startJobs();
const app = express()

app.use(cors())
app.use(bodyParser.json());

app.use("/webhooks/answer", answer);
app.use("/webhooks/events", events);
app.use("/webhooks/processVoice", processVoice);
app.use("/jobs", jobs);

app.listen(SERVER_PORT, () => {
  console.log(
    `🚀 The excuser app listening at http://localhost:${SERVER_PORT}`
  );
});
