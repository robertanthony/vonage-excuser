import converter from "number-to-words";

export const getArrayOfNumberWords = (n) => {
  const numArr = Array.from(Array(n).keys());
  return numArr.map((element) => converter.toWords(element));
};
