import { getArrayOfNumberWords } from "../utilities/index.js";


const minutesContext = getArrayOfNumberWords(120);



export const getMinutesNCCO = (request) => [
  {
    action: "talk",
    text: "Hello!",
  },
  {
    action: "talk",
    text: "How many minutes from now shall I call you back?",
    bargeIn: false,
  },
  {
    eventUrl: [
      `${request.protocol}://${request.get(
        "host"
      )}/webhooks/processVoice/processMinutes`,
    ],
    eventMethod: "POST",
    action: "input",
    type: ["speech"],
    endOnSilence: 1,
    speech: {
      language: "en-gb",
      context: minutesContext,
    },
  },
];

export const getGoodByeNCCO = (request) => [
  {
    action: "talk",
    text: "OK, thanks for using the Excuser! Good-bye for now!",
  },
];

export const getFinalProcessNCCO = (request, minutes) => [
  {
    action: "talk",
    text: "Great!",
  },
  {
    action: "talk",
    text: `I'll call you back in ${minutes} minutes? Please answer yes or no.`,
    bargeIn: false,
  },
  {
    eventUrl: [
      `${request.protocol}://${request.get(
        "host"
      )}/webhooks/processVoice/finalProcess`,
    ],
    eventMethod: "POST",
    action: "input",
    type: ["speech"],
    endOnSilence: 1,
    speech: {
      language: "en-gb",
      context: ["yes", "no"],
    },
  },
];

export const getRestartNCCO = (request) => [
  {
    action: "talk",
    text: "OK, let's try again!",
  },
  {
    action: "talk",
    text: "How many minutes from now should I call you back?",
    bargeIn: false,
  },
  {
    eventUrl: [
      `${request.protocol}://${request.get(
        "host"
      )}/webhooks/processVoice/processMinutes`,
    ],
    eventMethod: "POST",
    action: "input",
    type: ["speech"],
    speech: {
      language: "en-gb",
      context: minutesContext,
    },
  },
];

export const getProblemRestartNCCO = (request) => [
  {
    action: "talk",
    text: "I'm afraid I didn't understand you.  Let's try it again!",
  },
  {
    action: "talk",
    text: "How many minutes from now should I call you back?",
    bargeIn: false,
  },
  {
    eventUrl: [
      `${request.protocol}://${request.get(
        "host"
      )}/webhooks/processVoice/processMinutes`,
    ],
    eventMethod: "POST",
    action: "input",
    type: ["speech"],
    speech: {
      language: "en-gb",
      context: minutesContext,
    },
  },
];
