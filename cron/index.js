import cron from "node-cron";
import { getJobsToDo } from "../db/index.js";
import { makeOutboundCall } from "../outbound/index.js";

export const startJobs = (_) => {
  cron.schedule("*/5 * * * * *", () => {

const jobsToDo = getJobsToDo();
jobsToDo.forEach(job => {
    makeOutboundCall(job.to)
})



  });
};
