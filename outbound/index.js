const { API_KEY, API_SECRET, APPLICATION_ID, VONAGE_NUMBER } = process.env;

import Vonage from "@vonage/server-sdk";

export const makeOutboundCall = (toNumber) => {
  
  if (!toNumber) return;

  const vonage = new Vonage({
    apiKey: API_KEY,
    apiSecret: API_SECRET,
    applicationId: APPLICATION_ID,
    privateKey: "./private.key",
  });

  vonage.calls.create(
    {
      to: [
        {
          type: "phone",
          number: toNumber,
        },
      ],
      from: {
        type: "phone",
        number: VONAGE_NUMBER,
      },
      ncco: [
        {
          action: "talk",
          text: "Hi, just to remind you of your appointment!",
        },
      ],
    },
    (error, response) => {
      if (error) console.error(error);
    }
  );
};
