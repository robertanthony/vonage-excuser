import express from "express";
const routes = express.Router();

import { getAllJobs, addJobExternal, deleteJob } from "../db/index.js";

const addJob = (request, response) => {
  const job = request.body?.job;
  if (!job) return;
  addJobExternal(job.time, job.to)
  response.status(200).send('ok');
};

const delJob = (request, response) => {
  const id = request.body?.id;
  if (!id) return;
  deleteJob(id)
  response.status(200).send(`job ${id} deleted`);
};


const allJobs = (request, response) => {
  const jobs = getAllJobs();
  response.json(jobs);
};

routes.get("/alljobs", allJobs);
routes.post("/deljob", delJob);
routes.post("/addjob", addJob);

export default routes;



