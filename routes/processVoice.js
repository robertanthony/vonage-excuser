import express from "express";

import { addJob } from "../db/index.js";
import {
  getFinalProcessNCCO,
  getGoodByeNCCO,
  getProblemRestartNCCO,
  getRestartNCCO,
} from "../utilities/nccoFactory.js";

const routes = express.Router();

let minutes = "";
let from = "";

const checkSpeechResults = (request) =>
  request.body.speech &&
  request.body.speech.results &&
  request.body.speech.results.length > 0;

const processMinutes = (request, response) => {
  if (!checkSpeechResults(request))
    return response.json(getProblemRestartNCCO(request));

  const { results } = request.body.speech;

  minutes = request.body.speech.results[0].text;
  from = request.body.from;

  const ncco = getFinalProcessNCCO(request, minutes);
  response.json(ncco);
};

const finalProcess = (request, response) => {
  if (!checkSpeechResults(request))
    return response.json(getProblemRestartNCCO(request));
  const { results } = request.body.speech;
  if (
    results.some((element) => {
      return element.text === "yes";
    })
  ) {
    addJob(minutes, from);
    return response.json(getGoodByeNCCO(request));
  } else {
    const ncco = getRestartNCCO(request);
    response.json(ncco);
  }
};

routes.post("/processMinutes", processMinutes);
routes.post("/finalProcess", finalProcess);

export default routes;
