import express from "express";
import { getMinutesNCCO } from "../utilities/nccoFactory.js";

const routes = express.Router();

const incoming = (request, response) => {
  const ncco = getMinutesNCCO(request);
  
  response.json(ncco);
};

routes.get("/", incoming);

export default routes;
