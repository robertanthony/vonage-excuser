import express from "express";
const routes = express.Router();

const eventResponse = (request, response) => {
  return response.status(204).send();
};

routes.post("/", eventResponse);

export default routes;
