import { v4 as uuidv4 } from "uuid";
import { addMinutes, parseJSON, compareDesc } from "date-fns";
import lodash from "lodash";
import { wordsToNumbers } from "words-to-numbers";

const fakeDB = [];

export const addJob = (minutesAsString, fromNumber) => {
  const minutes = wordsToNumbers(minutesAsString);

  const now = new Date();
  const when = addMinutes(now, minutes);

  fakeDB.push({
    time: when.toJSON(),
    uuid: uuidv4(),
    to: fromNumber,
  });
};

export const addJobExternal = (dateJSON, fromNumber) => {
  if (!dateJSON || !fromNumber) {
    console.error("error in adding job");
    return;
  }

  fakeDB.push({
    time: dateJSON,
    uuid: uuidv4(),
    to: fromNumber,
  });
};

export const deleteJob = (id) => {
  const [deletedJob, newDB] = lodash.partition(fakeDB, (job) => {
    
   return job.uuid === id;
  });
  
  fakeDB.splice(0, fakeDB.length, ...newDB);
  

};

export const getJobsToDo = (_) => {
  const now = new Date();
  const [filtered, newDB] = lodash.partition(fakeDB, (job) => {
    const when = parseJSON(job.time);
    return compareDesc(when, now) === 1;
  });
  fakeDB.splice(0, fakeDB.length, ...newDB);
  return filtered;
};

export const getAllJobs = (_) => fakeDB;
