# The Excuser Web App

A simple Vonage voice app that allows the user to call in and schedule a callback.  At present, it accepts a specific number of minutes from the current time for the callback time.  The app uses voice recognition to schedule the return call.  It also provides endpoints to add a call, delete a scheduled call, and to see all scheduled calls.

## .ENV

Create an `.env` file at the root level of the project.  Add appropriate variables as per the file `env.template`


## Prepare

`yarn`





## Run

`yarn dev`

Runs the app on the port specified in the .env file

## Requires

Node v. 15.x
